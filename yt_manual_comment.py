import requests
import json
import time
import argparse
import os
from db import DBExecutor
from pprint import pprint
from datetime import datetime,timezone
from random import randint
from stem import Signal
from stem.control import Controller
from dateutil.relativedelta import relativedelta
from fake_useragent import UserAgent
import config as cfg



with Controller.from_port(port=9051) as c:
    c.authenticate()
    c.signal(Signal.NEWNYM)

# Code is partially grabbed from this repository:
# https://github.com/egbertbouman/youtube-comment-downloader


# PROXY = {'http': 'http://103.143.206.118:443'}

start_time = time.time()
ids_again = []
videoIds_again = []
run_again = False


def search_dict(partial, key):
    """
    A handy function that searches for a specific `key` in a `data` dictionary/list
    """
    if isinstance(partial, dict):
        for k, v in partial.items():
            if k == key:
                # found the key, return the value
                yield v
            else:
                # value of the dict may be another dict, so we search there again
                for o in search_dict(v, key):
                    yield o
    elif isinstance(partial, list):
        # if the passed data is a list
        # iterate over it & search for the key at the items in the list
        for i in partial:
            for o in search_dict(i, key):
                yield o


def time_converter(raw_time):
    d1 = datetime.now(timezone.utc).astimezone()
    raw_time = raw_time.split(' ')

    if (raw_time[1] == 'second') or (raw_time[1] == 'seconds'):
        time_ = datetime.now() - relativedelta(seconds=int(raw_time[0]))

    elif (raw_time[1] == 'minutes') or (raw_time[1] == 'minute'):
        time_ = datetime.now() - relativedelta(minutes=int(raw_time[0]))

    elif (raw_time[1] == 'hour') or (raw_time[1] == 'hours'):
        time_ = datetime.now() - relativedelta(hours=int(raw_time[0]))

    elif (raw_time[1] == 'day') or (raw_time[1] == 'days'):
        time_ = datetime.now() - relativedelta(days=int(raw_time[0]))

    elif (raw_time[1] == 'week') or (raw_time[1] == 'weeks'):
        time_ = datetime.now() - relativedelta(weeks=int(raw_time[0]))

    elif (raw_time[1] == 'month') or (raw_time[1] == 'months'):
        time_ = datetime.now() - relativedelta(months=int(raw_time[0]))

    elif (raw_time[1] == 'year') or (raw_time[1] == 'years'):
        time_ = datetime.now() - relativedelta(years=int(raw_time[0]))

    return time_.replace(microsecond=0).isoformat()



def find_value(html, key, num_sep_chars=2, separator='"'):
    # define the start position by the position of the key +
    # length of key + separator length (usually : and ")
    start_pos = html.find(key) + len(key) + num_sep_chars
    # the end position is the position of the separator (such as ")
    # starting from the start_pos
    end_pos = html.find(separator, start_pos)
    # return the content in this range
    return html[start_pos:end_pos]


def get_comments(video_id, id, run_again):
    session = requests.Session()
    proxies = {
        'http': 'socks5://127.0.0.1:9050',
        'https': 'socks5://127.0.0.1:9050'
    }


    headers = {'User-Agent': UserAgent().random,
               "Accept-Language": "en-US,en;q=0.5",
               "x-youtube-client-name": "1",
               "x-youtube-client-version": "2.20200731.02.01"
               }

    session.proxies.update(proxies)
    session.headers.update(headers)
    # make the request
    url = 'https://www.youtube.com/watch?v=' + video_id
    res = session.get(url=url)
    # print('response ' + str(res.text))
    print('url ' + str(url))
    if not 'window["ytInitialData"] =' in res.text:
        # Youtube not response because many request
        print('WWWWWWWWWWWarning : Youtube not reponse!!!')
        time_delay = randint(2, 5)
        print('delay ' + str(time_delay) + ' second')
        time.sleep(time_delay)

        res = session.get(url=url)
        if not 'window["ytInitialData"] =' in res.text:
            if not run_again:
                videoIds_again.append(video_id)
                ids_again.append(id)
                print('list again ' + str(len(videoIds_again)))
            return





    # extract the XSRF token
    xsrf_token = find_value(res.text, "XSRF_TOKEN", num_sep_chars=3)
    # parse the YouTube initial data in the <script> tag
    data_str = find_value(res.text, 'window["ytInitialData"] = ', num_sep_chars=0, separator="\n").rstrip(";")


    # print('data_str ' + str(data_str))countBreak
    # convert to Python dictionary instead of plain text string
    data = json.loads(data_str)


    # search for the ctoken & continuation parameter fields
    pagination_data = {}
    for r in search_dict(data, "itemSectionRenderer"):
        pagination_data = next(search_dict(r, "nextContinuationData"))
        if pagination_data:
            # if we got something, break out of the loop,
            # we have the data we need
            break
    if not ('continuation' in pagination_data and 'clickTrackingParams' in pagination_data):
        return
    continuation_tokens = [(pagination_data['continuation'], pagination_data['clickTrackingParams'])]

    while continuation_tokens:
        # keep looping until continuation tokens list is empty (no more comments)
        continuation, itct = continuation_tokens.pop()

        # construct params parameter (the ones in the URL)
        params = {
            "action_get_comments": 1,
            "pbj": 1,
            "ctoken": continuation,
            "continuation": continuation,
            "itct": itct,
        }

        # construct POST body data, which consists of the XSRF token
        data = {
            "session_token": xsrf_token,
        }

        # construct request headers

        # make the POST request to get the comments data
        response = session.post("https://www.youtube.com/comment_service_ajax", params=params, data=data,
                                headers=headers)
        # print('response ' + str(response.text))
        # convert to a Python dictionary
        comments_data = json.loads(response.text)

        count_texts = search_dict(comments_data, "countText")

        comment_count = 0
        for count_text in count_texts:
            comment_count_str = str(count_text['runs'][0]['text']).replace(',','').replace('.','')
            print('comment_count_str ' + str(comment_count_str))
            comment_count_str = comment_count_str.replace('\xa0', ' ').split(' ')[:-1]
            comment_count = "".join(comment_count_str)
            break

        # print('countText type ' + str(type(countText)))
        for comment in search_dict(comments_data, "commentRenderer"):
            # iterate over loaded comments and yield useful info
            totalReplyCount = 0
            if 'replyCount' in comment:
                totalReplyCount = comment["replyCount"]
            yield {
                "videoId": video_id,
                "c_id": comment["commentId"],
                "textOriginal": ''.join([c['text'] for c in comment['contentText']['runs']]),
                "publishedAt": time_converter(comment['publishedTimeText']['runs'][0]['text']),
                "isLiked": comment["isLiked"],
                "likeCount": comment["likeCount"],
                "totalReplyCount": totalReplyCount,
                'authorDisplayName': comment.get('authorText', {}).get('simpleText', ''),
                'authorChannelUrl': comment['authorEndpoint']['browseEndpoint']['browseId'],
                'votes': comment.get('voteCount', {}).get('simpleText', '0'),
                'photo': comment['authorThumbnail']['thumbnails'][-1]['url'],
                "authorIsChannelOwner": comment["authorIsChannelOwner"],
                'commentCount': comment_count,
            }

        # load continuation tokens for next comments (ctoken & itct)
        continuation_tokens = [(next_cdata['continuation'], next_cdata['clickTrackingParams'])
                               for next_cdata in
                               search_dict(comments_data, 'nextContinuationData')] + continuation_tokens

        # avoid heavy loads with popular videos
        time.sleep(0.5)


def save_comment_to_array(item):
    print('save_comment_to_array ' + str(item))
    comment = [
        str(item['c_id']),
        str(item['videoId']),
        str(item['authorDisplayName'].encode('utf-8', 'replace').decode('utf-8')),
        str(item['authorChannelUrl']),
        str(item['publishedAt']).replace('T', '-').replace('Z', ''),
        str(datetime.today().strftime('%Y-%m-%d-%H:%M:%S'))  # time crawl
    ]

    return {'comment': comment}


def save_comment_statistic_to_array(item):
    comment_statistic = [
        str(item['c_id']),
        str(item['textOriginal'].encode('utf-8', 'replace').decode('utf-8')),
        int(item['likeCount']),
        int(item['totalReplyCount']),
        str(datetime.today().strftime('%Y-%m-%d-%H:%M:%S')),  # time crawl
        # str(item['textOriginal'].encode('utf-8', 'replace').decode('utf-8')),
        # int(item['likeCount']),
        # int(item['totalReplyCount']),
    ]

    return {'comments_statistic': comment_statistic}


def save_video_statistic_to_array(count_comment, count_comment_crawl, id):
    print('save_video_statistic_to_array:  id = {}, count_comment = {}, count_comment_crawl = {}'
          .format(str(id),str(count_comment),str(count_comment_crawl)))
    videos_statistic = [
        int(count_comment),
        int(count_comment_crawl),
        str(datetime.today().strftime('%Y-%m-%d-%H:%M:%S')),  # time crawl
        str(id)

    ]

    return {'videos_statistic': videos_statistic}

def extract_comment(ids, videoIds):


    comments_info_result = []
    comments_statistic_result = []
    videos_statistic = []

    count_to_finish = 1
    if run_again:
        print('len_videoIds_again ' + str(len(videoIds)))
    else:
        print('len_videoIds ' + str(len(videoIds)))
    for index, videoId in enumerate(videoIds):
        print('--------------------- {} - {}-----------------------'.format(str(index), str(run_again)))
        count_comment = 0
        count_comment_crawl = 0
        id = ids[index]

        list_comments = get_comments(videoId, id, run_again)
        try:
            for count, comment in enumerate(list_comments):
                if count_comment == 0:
                    count_comment = comment['commentCount']

                comments_info_result.append(save_comment_to_array(item=comment)['comment'])
                comments_statistic_result.append(save_comment_statistic_to_array(item=comment)['comments_statistic'])
                count_comment_crawl = count

                if cfg.LIMIT_COMMENT and count >= cfg.LIMIT_COMMENT:
                    # break out of the loop when we exceed limit specified
                    break

        except Exception as e:
            print(e)

        if int(count_comment) > 0 or int(count_comment_crawl) > 0:
            videos_statistic.append(
                save_video_statistic_to_array(count_comment=count_comment, count_comment_crawl=count_comment_crawl,
                                          id=id)['videos_statistic']
            )

        print("total comments extracted:", count_comment)

        # save comment to oracle
        if len(videos_statistic) > 0:
            db_executor.save_comment_to_oracle(comments_info_result)
            db_executor.save_comment_statistic_to_oracle(comments_statistic_result)
            db_executor.save_video_statistic_to_oracle(videos_statistic)

            comments_info_result = []
            comments_statistic_result = []
            videos_statistic = []


        time.sleep(randint(1, 3))

        count_to_finish = count_to_finish + 1
        if count_to_finish > cfg.SIZE_BATCH_VIDEO:
            with open('report.csv', 'a+') as f:
                f.write("\nFinish : " + str(cfg.SIZE_BATCH_VIDEO) + ' videos')
            break


if __name__ == "__main__":

    db_executor = DBExecutor()
    ids, videoIds = db_executor.get_video_for_increase_comment()


    extract_comment(ids, videoIds)

    if not run_again and len(videoIds) > 0:
        run_again = True
        extract_comment(ids_again, videoIds_again)


    db_executor.close()

    with open('report.csv', 'a+') as f:

        f.write("\n Total Video : " + str(len(videoIds)) + ' videos')
        f.write("\n Time eslapsed: " + str(time.time() - start_time) + '')
        f.write("\n Time start: " + datetime.fromtimestamp(start_time).strftime('%Y-%m-%d-%H:%M:%S'))
        f.write("\n Time end: " + str(datetime.today().strftime('%Y-%m-%d-%H:%M:%S')) + '')
        f.write("\n====================================\n")


