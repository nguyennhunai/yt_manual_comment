import json
import logging
import os
import random
from datetime import date

import coloredlogs

import config as cfg
import cx_Oracle

logger = logging.getLogger("db executor")
logger.setLevel(logging.WARNING)
coloredlogs.install(logger=logger,
                    fmt='%(asctime)s %(threadName)s %(lineno)d %(levelname)s %(message)s')

MAX_USER_EACH_PROVINCE = int(os.getenv("MAX_USER_EACH_PROVINCE", 500))


def get_db_connection():
    conn = cx_Oracle.connect(
        "{}/{}@{}/{}".format(cfg.ORACLE_NAME,
                             cfg.PASSWORD,
                             cfg.IP,
                             cfg.SERVICE_NAME),
        encoding="UTF-8",
        nencoding="UTF-8",
    )
    return conn


class DBExecutor(object):
    def __init__(self, username=cfg.ORACLE_NAME, password=cfg.PASSWORD, ip=cfg.IP, srv_name=cfg.SERVICE_NAME,
                 conn_type='SERVICE_NAME', port=1521):
        dsn_tns = """
                  (DESCRIPTION =
                  (ADDRESS = (PROTOCOL = TCP)(HOST = {})(PORT = {}))
                  (CONNECT_DATA =
                    (SERVER = DEDICATED)
                    ({} = {})
                  )
                  )
              """.format(ip, port, conn_type, srv_name)

        self.__dsn_tns = dsn_tns
        self.__user = username
        self.__password = password
        self.pool = None
        # self._create_pool()

        self.__conn = cx_Oracle.connect(
            "{}/{}@{}/{}".format(cfg.ORACLE_NAME,
                                 cfg.PASSWORD,
                                 cfg.IP,
                                 cfg.SERVICE_NAME),
            encoding="UTF-8",
            nencoding="UTF-8",
        )
        self.TABLE_NAME = ['YOUTUBE_MANUAL_COMMENTS', 'YOUTUBE_MANUAL_VIDEOS',
                           'YOUTUBE_MANUAL_VIDEOS_STATISTIC', 'YOUTUBE_MANUAL_COMMENTS_STATISTIC']

    def close(self):
        self.__conn.close()

    def _create_pool(self):
        if self.pool is None:
            self.pool = cx_Oracle.SessionPool(
                user=self.__user,
                password=self.__password,
                dsn=self.__dsn_tns,
                encoding="UTF-8",
                nencoding="UTF-8",
                min=2,
                max=4,
                increment=1,
                maxLifetimeSession=1800,
                timeout=30,
                threaded=True
            )


    def get_video_for_statistic(self):
        """
        get from db videoId in create_time > now - 3 day, set them as start_urls
        :return: post links
        """
        cursor = self.__conn.cursor()
        try:
            sql = "select distinct video_id " \
                  "from youtube_manual_videos " \
                  "where time_create > sysdate - 3"
            fetch_data = cursor.execute(sql).fetchall()
            fetch_data = ["" + d[0] for d in fetch_data]
            return fetch_data
        except Exception as e:
            raise e
        finally:
            cursor.close()

    def get_video_for_increase_comment(self):
        """
        get from db videoId in create_time > now - 3 day, set them as start_urls
        :return: post links
        """
        cursor = self.__conn.cursor()
        try:
            sql = "select distinct id, video_id, time_crawl " \
                  " from youtube_manual_videos_statistic" \
                  " WHERE (video_id, time_crawl) IN " \
                  " (SELECT video_id, MAX(time_crawl)" \
                  " FROM youtube_manual_videos_statistic" \
                  " GROUP BY video_id) " \
                  " and time_create > sysdate - 3 " \
                  " and comments_crawl < 40" \
                  " order by time_crawl asc " \

            fetch_data = cursor.execute(sql).fetchall()

            ids = ["" + d[0] for d in fetch_data]
            videoIds = ["" + d[1] for d in fetch_data]
            return ids, videoIds
        except Exception as e:
            raise e
        finally:
            cursor.close()

    def save_video_statistic_to_oracle(self, videos_statistic):
        videos_statistic_error = []
        print('+++++++++++++++++++ {} +++++++++++++++++++'.format('save_video_statistic_to_oracle'))
        print('len save_comment_to_oracle ' + str(len(videos_statistic)))
        # print('save_video_statistic_to_oracle ' + str(videos_statistic))

        sql = "update {} " \
              "set COMMENTS = :1" \
              ", COMMENTS_CRAWL = :2" \
              ", TIME_CRAWL = to_timestamp_tz(:3,'YYYY-MM-DD HH24:MI:SS.FF')" \
              " where id = :4".format(self.TABLE_NAME[2])

        cursor = self.__conn.cursor()
        cursor.executemany(sql, videos_statistic, batcherrors=True)
        for error in cursor.getbatcherrors():
            videos_statistic_error.append("Error" + str(error.message) + "at row offset" + str(error.offset))
        self.__conn.commit()

        with open('pipelines_videos_statistic_errors.txt', 'a+') as f:
            for error in cursor.getbatcherrors():
                f.write("Error" + str(error.message) + "at row offset" + str(error.offset) + '\n')
            for error in videos_statistic_error:
                f.write('\n' + str(error))
        cursor.close()
        print("insert success db save_video_statistic_to_oracle")

    def save_comment_to_oracle(self, comments):
        comment_errors = []

        print('+++++++++++++++++++ {} +++++++++++++++++++'.format('save_comment_to_oracle'))
        print('len save_comment_to_oracle ' + str(len(comments)))
        # print('save_comment_to_oracle ' + str(comments))

        sql = "insert into {} (" \
              "COMMENTS_ID, VIDEO_ID, " \
              "AUTHOR_DISPLAY_NAME, AUTHOR_CHANNEL_URL, " \
              "TIME_UPLOAD, " \
              "TIME_CRAWL, TIME_CREATE) " \
              "values (:1, :2, :3, :4, to_timestamp_tz(:5,'YYYY-MM-DD HH24:MI:SS.FF')," \
              " to_timestamp_tz(:6,'YYYY-MM-DD HH24:MI:SS.FF')" \
              ", to_timestamp_tz(:6,'YYYY-MM-DD HH24:MI:SS.FF'))".format(
            self.TABLE_NAME[0])

        cursor = self.__conn.cursor()
        cursor.executemany(sql, comments, batcherrors=True)
        self.__conn.commit()

        with open('pipelines_comment_errors.txt', 'a+') as f:
            for error in cursor.getbatcherrors():
                f.write("Error" + str(error.message) + " at row offset " + str(error.offset) + '\n')
            for error in comment_errors:
                f.write('\n' + str(error))

        cursor.close()
        print("insert success db save_comment_to_oracle")

    def save_comment_statistic_to_oracle(self, comments_statistic):
        comment_errors = []
        # #print(str(json.dumps(comments)))
        print('len save_comment_statistic_to_oracle ' + str(len(comments_statistic)))
        print('+++++++++++++++++++ {} +++++++++++++++++++'.format('save_comment_statistic_to_oracle'))
        # print('save_comment_statistic_to_oracle ' + str(comments_statistic))

        sql = "insert into {} (" \
              "COMMENTS_ID, TEXT_ORIGINAL, " \
              "LIKE_COUNT, TOTAL_REPLY_COUNT, " \
              "TIME_CRAWL, TIME_CREATE) " \
              "values (:1, :2, :3,:4 ,to_timestamp_tz(:5,'YYYY-MM-DD HH24:MI:SS.FF')" \
              ",to_timestamp_tz(:5,'YYYY-MM-DD HH24:MI:SS.FF'))".format(
            self.TABLE_NAME[3])

        cursor = self.__conn.cursor()
        cursor.executemany(sql, comments_statistic, batcherrors=True)
        self.__conn.commit()

        with open('pipelines_comment_statistic_errors.txt', 'a+') as f:
            for error in cursor.getbatcherrors():
                f.write("Error" + str(error.message) + " at row offset " + str(error.offset) + '\n')
            for error in comment_errors:
                f.write('\n' + str(error))
        cursor.close()
        print("insert success db save_comment_statistic_to_oracle")

    def get_post_link_again(self):
        """
        get from db all post link has create_time > now - 3 day, set them as start_urls
        :return: post links
        """
        cursor = self.__conn.cursor()
        try:
            sql = "select distinct post_id FROM province_user_post_reactions"
            fetch_data = cursor.execute(sql).fetchall()
            fetch_data = ["https://mbasic.facebook.com/" + d[0] for d in fetch_data]
            return fetch_data
        except Exception as e:
            raise e
        finally:
            cursor.close()

    def get_page_post_link(self):
        """
        get from db all post link has create_time > now - 3 day, set them as start_urls
        :return: post links
        """
        cursor = self.__conn.cursor()
        try:
            sql = "select id from page_posts" \
                  " where created_time > sysdate - 3"
            fetch_data = cursor.execute(sql).fetchall()
            fetch_data = ["https://mbasic.facebook.com/" + str(d) for d, in fetch_data]
            print("got {} posts from FB".format(len(fetch_data)))
            return fetch_data
        except Exception as e:
            raise e
        finally:
            cursor.close()

    def get_post_link(self):
        """
        get from db all post link has create_time > now - 3 day, set them as start_urls
        :return: post links
        """
        cursor = self.__conn.cursor()
        try:
            sql = "select permalink_url from province_user_posts" \
                  " where created_time > sysdate - 3 and monitor = 1"
            fetch_data = cursor.execute(sql).fetchall()
            fetch_data = [d[0].replace("facebook.com", "mbasic.facebook.com") for d in fetch_data]
            print("got {} posts from FB".format(len(fetch_data)))
            return fetch_data
        except Exception as e:
            raise e
        finally:
            cursor.close()

    def select(self, cols, table, conditions=[], to_dict=False):
        cursor = self.__conn.cursor()
        fetch_data = []
        try:
            cols_str = ",".join(cols)
            sql = "select {} from {}".format(cols_str, table)
            if len(conditions) > 0:
                condition_str = " where "
                for idx, c in enumerate(conditions):
                    condition_str += " {} {} {} ".format(c[0], c[1], c[2])
                    if not idx == len(conditions) - 1:
                        condition_str += " and "
                sql += condition_str
            print(sql)
            fetch_data = cursor.execute(sql).fetchall()
            if to_dict:
                fetch_data = [dict(zip(cols, d)) for d in fetch_data]
        except Exception as e:
            print(e)
        finally:
            cursor.close()
        return fetch_data

    def get_priority_user_uids_top(self, rate, province_code, fill_mode=1):
        """
        Get ids from DB, with 0.5 highest, and 0.5 random the rest, get record = rate * num of row
        :param fill_mode:
        :param province_code:
        :param rate : float rate get, ex 0.5, 0.7
        :return: list of (fbuid, province)
        """
        in_vars = ','.join(':%d' % i for i in range(len(province_code)))
        if fill_mode == 1:
            sql = "select fbuid, province, province_code from province_persons where " \
                  "last_time_crawl is null and " \
                  "verify_home = 1 and " \
                  "province_code in (%s) and " \
                  "available = 1" % in_vars
        elif fill_mode == 0:
            sql = "select fbuid, province, province_code from province_persons where " \
                  "verify_home = 1 and " \
                  "province_code in (%s) and " \
                  "available = 1 and " \
                  "(last_time_crawl <= sysdate - 2 or last_time_crawl is null) " \
                  "order by priority desc" % in_vars
        # print(sql)
        data_full = []
        fetch_data = self.fetch_data_raw_sql(sql, params=province_code)
        data_len = len(fetch_data)
        print("len data = {}".format(data_len))
        if data_len <= MAX_USER_EACH_PROVINCE:
            return fetch_data
        num_row = int(rate * int(len(fetch_data)))
        if num_row < MAX_USER_EACH_PROVINCE:
            num_row = min(MAX_USER_EACH_PROVINCE, len(fetch_data))
        print("max num row = ", num_row)
        data1 = fetch_data[:int(num_row / 2)]
        data_full += data1
        data2 = fetch_data[int(num_row / 2):len(fetch_data)]
        data2 = random.sample(data2, int(num_row / 2))
        data_full += data2
        return data_full

    def fetch_data_raw_sql(self, sql, params):
        """
        Fetch data from DB with sql query
        :param sql: string sql
        :return: list of data
        """
        # logger.info("fetch_data {}".format(sql))
        cursor = None
        res = None
        try:
            cursor = self.__conn.cursor()
            res = cursor.execute(sql, params).fetchall()
        except cx_Oracle.DatabaseError as e:
            logger.warning("There is a problem with Oracle", e)
        finally:
            if cursor:
                cursor.close()
        return res
